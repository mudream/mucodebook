#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>

using namespace std;

template<typename T>
struct _RefCounter{
    T data;
    int ref;
    _RefCounter(const T&d=nullptr):data(d),ref(0){}
    void DecRefX(){
        if(this == nullptr) return;
        ref--;
        if(ref == 0)
            delete this;
        return;
    }
    void IncRefX(){
        if(this == nullptr) return;
        ref++;
        return;
    }
};

template<typename T>
struct reference_pointer{
    _RefCounter<T> *p;
    T *operator->(){
        return &(p->data);
    }
    T &operator*(){
        return p->data;
    }
    reference_pointer&operator=(const reference_pointer &t){
        p->DecRefX();
        p = t.p;
        p->IncRefX();
        return *this;
    }
    bool IsNull(){
        return p == nullptr;
    }
    void SetNull(){
        p->DecRefX();
        p = nullptr;
        return;
    }
    reference_pointer(_RefCounter<T> *t=nullptr):p(t){
        p->IncRefX();
        return; 
    }
    reference_pointer(const reference_pointer &t):p(t.p){
        p->IncRefX();
        return;
    }
    ~reference_pointer(){
        p->DecRefX();
        return;
    }
};

template<typename T,typename... _Args>
inline reference_pointer<T>
new_reference(_Args... __args){
    return reference_pointer<T>
        (new _RefCounter<T>(T(__args...)));
}

struct Jeap;
typedef reference_pointer<Jeap> ptrJeap;

struct Jeap{
    ptrJeap l, r;
    int pri; char val; int sz;
    
    Jeap(char c = 0){
        sz = 1;
        val = c;
        return;
    }

    int Size(){
        return (this == nullptr) ? 0:sz;
    }

    void Update(){
        sz = 1 + l->Size() + r->Size();
        return;
    }

    void Print(){
        if(this == nullptr) return;
        l->Print();
        putchar(val);
        r->Print();
        return;
    }

};

ptrJeap Cp(ptrJeap a){
    if(a.p == nullptr)
        return a;
    ptrJeap r = new_reference<Jeap>();
    *r = *a;
    return r;
}

ptrJeap Merge(ptrJeap a, ptrJeap b){
    if(a.IsNull()) return Cp(b);
    if(b.IsNull()) return Cp(a);
    static int x;
    if((x++)%(a->Size() + b->Size()) < a->Size()){
        a = Cp(a);
        a->r = Merge(a->r, b);
        a->Update();
        return a;
    }else{
        b = Cp(b);
        b->l = Merge(a, b->l);
        b->Update();
        return b;
    }
}

void Split(ptrJeap org, int low, ptrJeap &a, ptrJeap &b){
    if(org.IsNull()){
        a.SetNull();
        b.SetNull();
    }else if(org->l->Size() < low){
        a = Cp(org);
        Split(org->r, low - org->l->Size() - 1, a->r, b);
        a->Update();
    }else{
        b = Cp(org);
        Split(org->l, low, a, b->l);
        b->Update();
    }
    return;
}

/* pass list:
 * HOJ 226
 */
