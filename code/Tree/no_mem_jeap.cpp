#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>

using namespace std;

inline int ran(){
    static int x=15180255;
    return x=(x*0xdefaced+1)&2147483647;
}

struct Jeap{
    Jeap* l;
    Jeap* r;
    int sz, val, pri;
    Jeap(int _v = 0){
        l = nullptr, r = nullptr;
        sz = 1;
        val = _v;
        pri = ran();
        return;
    }
    int Size(){
        if(this == nullptr) return 0;
        return sz;
    }
    void Update(){
        sz = l->Size() + r->Size() + 1;
        return;
    }
};

Jeap* Cp(Jeap* a){
    if(a == nullptr)
        return nullptr;
    Jeap* ret = new Jeap();
    *ret = *a;
    return ret;
}

Jeap* Merge(Jeap* a, Jeap* b){
    if(a == nullptr) return Cp(b);
    if(b == nullptr) return Cp(a);
    if(a->pri > b->pri){
        a = Cp(a);
        a->r = Merge(a->r, b);
        a->Update();
        return a;
    }else{
        b = Cp(b);
        b->l = Merge(a, b->l);
        b->Update();
        return b;
    }
}

void Split(Jeap* org, int low, Jeap* &a, Jeap* &b){
    if(org == nullptr) a = nullptr, b = nullptr;
    else if(org->l->Size() >= low){
        b = Cp(org);
        Split(org->l, low, a, b->l);
        b->Update();
    }else{
        a = Cp(org);
        Split(org->r, low-org->l->Size()-1, a->r, b);
        a->Update();
    }
    return;
}
