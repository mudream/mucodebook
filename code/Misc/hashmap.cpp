#include <cstdio>
#include <cstdlib>
#include <unordered_map>

using namespace std;
struct key{int a, b; key(int _a, int _b):a(_a), b(_b){return;}};
bool operator==(const key& a, const key& b){return a.a == b.a and a.b == b.b;}

struct keyHasher{
    size_t operator()(const key& k)const{
        return (hash<int>()(k.a)<<15)^(hash<int>()(k.b));
    }
};

// test
int main(){
    unordered_map<key, int, keyHasher> test;
    test[key(1, 2)] = 3;
    test[key(4, 8)] = 6;
    printf("%d\n", test[key(1, 2)]);
    return 0;
}
