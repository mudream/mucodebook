// 51nod.com 1028

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

using namespace std;

typedef long long int int64;

const int64 N = 1 << 18;
const int64 P = (479 << 21) + 1;
const int64 G = 3;

int64 wn[20];
int64 a[N], b[N];
char A[N], B[N];
int len;

int64 mpow(int64 a, int64 n){
    if(n == 0) return 1;
    int64 val = mpow(a, n/2);
    val = (val*val)%P;
    if(n&1) return (val*a)%P;
    return val;
}

inline void setwn(){
    for(int lx = 0;lx < 20;lx++)
        wn[lx] = mpow(G, (P-1)/(1<<lx));
    return;
}

inline void prepare(){
    len = 1;
    int alen = strlen(A);
    int blen = strlen(B);
    while(len <= alen+blen+1) len<<=1;
    for(int lx = 0;lx < len;lx++)
         a[lx] = b[lx] = 0;
    for(int lx = 0;lx < alen;lx++)
        a[lx] = A[alen-lx-1]-'0';
    for(int lx = 0;lx < blen;lx++)
        b[lx] = B[blen-lx-1]-'0';
    return;
}

void Rader(int64* arr){
    int j = len>>1;
    for(int i = 1;i < len-1;i++){
        if(i<j) swap(arr[i], arr[j]);
        int k = len>>1;
        while(j >= k){
            j -= k;
            k >>= 1;
        }
        if(j < k) j += k;
    }
    return;
}

void NTT(int64* arr){
    Rader(arr);
    for(int h = 2, id = 1; h <= len; h<<=1, id++){
        for(int j = 0; j < len; j += h){
            int64 w = 1;
            for(int k = j; k < j + h/2;k++){
                int64 u = arr[k]%P;
                int64 t = w*(arr[k + h/2]%P)%P;
                arr[k] = (u + t)%P;
                arr[k + h/2] = ((u - t)%P + P)%P;
                w = (w*wn[id])%P;
            }
        }
    }
    return;
}

void Conv(int64* arr1, int64* arr2, int64* ret){
    NTT(arr1);
    NTT(arr2);
    for(int lx = 0;lx < len;lx++)
        ret[lx] = (arr1[lx]*arr2[lx])%P;
    NTT(ret);
    for(int lx = 1;lx < len/2; lx++)
        swap(ret[lx], ret[len - lx]);
    int64 inv = mpow(len, P-2);
    for(int lx = 0;lx < len;lx++)
        ret[lx] = (ret[lx]*inv)%P;
    return;
}
    
int main(){
    setwn();
    while(scanf("%s %s", A, B) == 2) {
        prepare();
        Conv(a, b, a);
        for(int lx = 1;lx < len;lx++){
            a[lx] += a[lx-1]/10;
            a[lx-1]%=10;
        }
        bool st = false;
        for(int lx = len-1;lx >= 0;lx--){
            if(a[lx] != 0) st = true;
            if(st) printf("%lld", a[lx]);
        }
        printf("\n");
    }
    return 0;
}
