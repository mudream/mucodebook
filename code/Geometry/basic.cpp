#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>

#define PI 3.14159265358979323846264338

using namespace std;

struct pt{ double x; double y; };
struct line{ pt p; pt slide; };
struct cir{pt p; double r;};
pt operator+(pt a, pt b){
    return {a.x + b.x, a.y + b.y};
}

pt operator*(pt a, double k){
    return {a.x*k, a.y*k};
}

pt operator-(pt a, pt b){
    return {a.x-b.x, a.y-b.y};
}

pt operator/(pt a, double k){
    return {a.x/k, a.y/k};
}

double len(pt a){
    return sqrt(a.x*a.x + a.y*a.y);
}

double CalTriArea(pt a, pt b, pt c){
    return fabs(a.x*b.y + b.x*c.y + c.x*a.y - a.x*c.y - b.x*a.y - c.x*b.y)/2;
} 

double CalOutCirRadius(pt a, pt b, pt c){
    double C = len(a-b),
           A = len(b-c),
           B = len(a-c);
    return A*B*C/(4*CalTriArea(a, b, c));
}
