// modify from http://stanford.edu/~liszt90/acm/notebook.html#file2
#include <vector>

using namespace std;

typedef vector<int> VI;
typedef vector<VI> VVI;

struct Bipart{
    VI mr, mc, seen;
    VVI G;
    int n, m;
    Bipart(int _n, int _m):n(_n), m(_m), G(VVI(_n, VI(_m, 0))){return;}
    
    void push_edge(int a, int b){G[a][b] = 1; return;}
    
    bool dfs(int i){
        for(int j = 0;j < m;j++){
            if(G[i][j] and !seen[j]){
                seen[j] = true;
                if(mc[j] < 0 or dfs(mc[j])){
                    mr[i] = j, mc[j] = i;
                    return true;
                }
            }
        }
        return false;
    }

    int run(){
        mr = VI(n, -1), mc = VI(m, -1);
        int ans = 0;
        for(int lx = 0;lx < n;lx++){
            seen = VI(m, 0);
            if(dfs(lx)) ans++;
        }
        return ans;
    }
};
