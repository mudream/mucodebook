#include <vector>

using namespace std;

typedef vector<int> VI;
typedef vector<VI> VVI;

const int inf = 1000000000;

struct dinic{
    struct edge{int a, b, c;};

    vector<edge> es;
    VVI G;
    VI Gptr, dis;

    int s, t, n;

    dinic(int _n):n(_n), G(VVI(_n, VI())){return;}

    void push_edge(int a, int b, int c){
        G[a].push_back(es.size()); es.push_back({a, b, c});
        G[b].push_back(es.size()); es.push_back({b, a, 0});
        return;
    }

    bool bfs(){
        dis = VI(n, -1);
        vector<int> que(n, 0);
        int qs = 0, qe = 1; que[0] = t, dis[t] = 0;
        while(qs < qe){
            int prc = que[qs++];
            for(int lx = 0;lx < G[prc].size();lx++){
                int eind = G[prc][lx];
                int nxt = es[eind].b;
                if(es[eind^1].c and dis[nxt] == -1){
                    que[qe++] = nxt;
                    dis[nxt] = dis[prc]+1;
                }
            }
        }
        return dis[s] != -1;
    }

    int dfs(int prc, int wei){
        if(!wei) return 0;
        if(prc == t) return wei;
        for(int& lx = Gptr[prc]; lx < G[prc].size();lx++){
            int eind = G[prc][lx];
            int nxt = es[eind].b;
            if(es[eind].c and dis[nxt] == dis[prc]-1){
                int gv = dfs(nxt, min(wei, es[eind].c));
                if(gv){
                    es[eind].c -= gv;
                    es[eind^1].c += gv;
                    return gv;
                }
            }
        }
        return 0;
    }

    int run(int _s, int _t){
        s = _s, t = _t;
        int ans = 0;
        while(bfs()){
            Gptr = VI(n, 0);
            for(;;){
                int gv = dfs(s, inf);
                if(!gv) break;
                ans += gv;
            }
        }
        return ans;
    }  
};
