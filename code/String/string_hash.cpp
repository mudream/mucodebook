#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

typedef long long int int64;
const int64 P1 = 1000000007;
const int64 X1 = 29;
int64 X1n[200010];

void Init(){
    X1n[0] = 1;
    for(int lx = 1;lx < 200005;lx++)
        X1n[lx] = (X1n[lx-1]*X1)%P1;
    return;
}

struct Hasher{
    int64* hv;
    Hasher(const char* str, int len = -1){
        if(len == -1) len = strlen(str);
        hv = new int64[len+1];
        hv[0] = 0;
        for(int lx = 0;lx < len;lx++)
            hv[lx+1] = ((hv[lx]*X1)%P1 + (int64)(str[lx] - 'a' + 1))%P1;
        return;
    }
    ~Hasher(){
        delete[] hv;
    }
    int64 query(int a, int b){
        a++, b++;
        int64 ret1 = hv[b] + P1 - (hv[a-1]*X1n[b-a+1])%P1;
        return ret1%P1;
    }
};

int main(){
    Init();
    Hasher h1("abcabc"), h2("cca");
    printf("%lld %lld\n", h1.query(2,3), h2.query(1, 2));
    return 0;
}
