/*
 * AC code: HDU 2222
*/

#include <cstdio>
#include <cstdlib>
#include <queue>

using namespace std;

struct Node{
    Node(){
        for(int lx = 0;lx < 26;lx++)
            child[lx] = nullptr;
        fail = nullptr;
        count = 0;
        visit = 0;
        return;
    }

    ~Node(){
        for(int lx = 0;lx < 26;lx++)
            if(child[lx] != nullptr)
                delete child[lx];
        return;
    }

    Node* child[26];
    Node* fail;
    int count;
    bool visit;
};

class AcTrie{
private:
    Node* root;
public:
    AcTrie(){
        root = new Node();
        return;
    }
    
    ~AcTrie(){
        delete root;
    }
     
    void Insert(const char* str){
        Node* prc = root;
        for(int lx = 0;str[lx];lx++){
            int id = str[lx]-'a';
            if(prc->child[id] == nullptr)
                prc->child[id] = new Node();
            prc = prc->child[id];
        }
        prc->count++;
        return;
    }

    void Make(){
        queue<Node*> que;
        que.push(root);
        while(que.size()){
            Node* prc = que.front(); que.pop();
            for(int lx = 0;lx < 26;lx++){
                if(prc->child[lx] == nullptr) continue;
                Node* rdc = prc->fail;
                while(rdc != nullptr and rdc->child[lx] == nullptr)
                    rdc = rdc->fail;
                if(rdc != nullptr) rdc = rdc->child[lx];
                if(rdc == nullptr) rdc = root;
                prc->child[lx]->fail = rdc;
                que.push(prc->child[lx]);
            }
        }
        return;
    }
    
    int Query(const char* str){
        Node* ptr = root;
        root->visit = true;
        int ans = 0;
        for(int lx = 0;str[lx];lx++){
            int id = str[lx]-'a';
            while(ptr->child[id] == nullptr and ptr != root)
                ptr = ptr->fail;
            ptr = ptr->child[id];
            if(ptr == nullptr) ptr = root;
            Node* tmp = ptr;
            while(not tmp->visit){
                ans += tmp->count;
                tmp->visit = true;
                tmp = tmp->fail;
            }
        }
        return ans;
    }
};

char buf[1000005];

int main(){
    int T; scanf("%d", &T);
    while(T--){
        AcTrie ac;
        int n; scanf("%d", &n);
        while(n--){
            scanf("%s", buf);
            ac.Insert(buf);
        }
        ac.Make();
        scanf("%s", buf);
        printf("%d\n", ac.Query(buf));
    }
    return 0;
}
